# Harmonize

- [Overview](#overview)
- [Purpose](#purpose)
- [Structure](#structure)
- [How To Use](#how-to-use)
- [Examples](#examples)
- [Composer](#composer)

## Overview
The best way to explain the use of this module is through this little
list. Do you identify with any of the below?

> I want to preprocess data for entities and fields easily, without
having to write 5000 lines in my `.theme` or `.module` file.

> I have a hard time accessing data for certain fields in certain
templates.

> I am a Back-End developer and the Front-End team is doing work in
their own component library installation. I need an easy way to bring it
all together.

## Purpose
The Harmonize module has one goal and one goal only: To seamlessly
harmonize communication between **Front-End** & **Back-End**.

It accomplishes this by streamlining data preprocessing and making it
much easier to do it, eliminating the need to create countless
preprocess hooks in a theme's `.theme` file.

A quick example: Accessing the URL of a link field in a node's twig
template would necessitate the following code:

```
{{ node.field_my_link.0.url }}
```

With Harmonize, you would only have to do the following:

```
{{ data.my_link.url }}
```

And that's only with the bare-bones configurations! The truth is, you
have a lot of control over the preprocessing of data. This will be
explained in further detail further down in this guide!

Doesn't seem too different? Well, a rougher example would be accessing
data in `entity_reference` or `media` fields where you're dealing with
nested entities. It'll be much cleaner than seeing a constant chain of
`field_my_media.0.entity.field_image.0.entity.image` and so on...

### Quick-Start Example Guide
Wanna see this module in action quick? Well, if you're starting off a
fresh installation, it's gonna take some setting up, as the module needs
working entities / content. Here's a list of steps.

1. Create a content type and add some fields to test with. Add many
different types of fields for more examples. (Paragraphs & Entity
References are supported as well).
2. Go to the main settings page. `/admin/config/harmonize/settings`.
3. Check the **Enabled Automatic Preprocessing** checkbox.
4. Add a node of the content type you created and populate it
with dummy data.
5. Rebuild the site cache.
6. Now in the template file associated to this entity type, you should
have data in a variable called `data`. Dump it and check out the
contents!

## How It Works

### Using the Harmonize Service
If you enable the module, the functionality won't be used automatically.
You can only really see the results if you use the module's main service
in the `.theme` file of your custom theme.

In the said file, here is an example snippet you could add.

```php
/**
 * Implements hook_preprocess_HOOK() for page document templates.
 */
function THEMENAME_preprocess_node(&$variables) {
  // Only harmonize nodes of type 'basic_page'.
  if ($variables['node']->bundle() === 'basic_page') {
    $variables['data'] = \Drupal::service('harmonize')->harmonize($variables['node']);
  }
}
```

The `harmonize()` functions accepts most entities and will return a set
of organized data.

With this example, in your `node--basic-page.html.twig`, you will now
have an available `data` variable that contains the harmonized data.
Take a look at how the data comes out. Clean, right?

### Activating Automatic Preprocessing
Visit the configuration page at `/admin/config/harmonize/settings`
to activate Automatic Preprocessing.

This makes it so that all basic entities (Nodes, Paragraphs & Media)
will be processed automatically and should have an available variable
called `data` in their twig template files without having to add any
code to your `.theme` file. This is like a dream come true for some,
avoiding 2000 lines of preprocessing in the `.theme` file.

### Harmonization Signature Concept
A Harmonization Signature represents the identification of **what** is
being harmonized. An example signature would be
`entity.node.basic_page`, indicating that the module is processing an
**Entity**, of type **Node**, with the **Basic Page** bundle. Another
example would be `field.email.field_email`. This applies to a **Field**,
of type **Email**, with the machine name **field_email**.

When using any of the modules features, if you target a signature like
`entity.node`, it works and will target all **Nodes** on your site.
This is important to note for when you start using **Refiners** &
**Harmony**. You'll learn more about those in just a second!

### Using Refiners
By default, the module preprocesses and outputs its data in the same way
no matter what kind of object or entity is given. That means the output
is pretty streamlined. But there are options for case by case
customizations.

**Refiners** are one of these options. They are **Plugins** that can be
implemented in a module or theme to alter the data the module returns.
This is symbolic to Drupal's native `hook_preprocess` functions.

A Refiner plugin's code looks a little like this:

```php
<?php

namespace Drupal\harmonize_example\Plugin\Harmonizer\Refiner;

use Drupal\harmonize\PluginManager\Refinery\RefinerBase;

/**
 * Provide plugin to alter harmonizer data for all Basic Pages.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Refiner(
 *   id = "harmonize_example.basic_page_refiner",
 *   target = "entity.node.basic_page",
 *   weight = 1,
 *   themes = {
 *     'MY_THEME',
 *   }
 * )
 *
 * @package Drupal\harmonize\Plugin\Harmonizer\Refiner
 */
final class BasicPageRefinerExample extends RefinerBase {

  /**
   * Add personal tweaks to data in this function.
   *
   * {@inheritdoc}
   */
  public function refine(&$consignment, &$harmony, $node): void {
    // Alter Harmonization Data here!
  }

}
```

Refer to the **harmonize_example** submodule for working examples.
You can base yourself on this code to create your own **Refiners**.

### Using Harmony File Discovery
**Harmony File Discovery** is a strategy that involves adding a
directory to your theme that allows for overrides of the data passed
by the module to templates in a highly segmented/organized collection of files. In other words, it's **just like Refiners**, but **way simpler** and meant to be easy to use and apply.

To get started, visit the configuration page at `/admin/config/harmonizer/configuration`.
Then, create a `./themes/custom/SITE_THEME/harmony` folder at the root of your **custom theme**.

This folder can be seen as the "agreement" between Front-End & Back-End teams. Here, a `*.harmony.php` file can be created for a given entity or object. This file only needs to contain php code that manipulates 2 variables:

- `$consignment` - The data returned by the module's first act of preprocessing (and Refiner preprocessing if any).
- `$harmony` - The data that is ultimately sent to the templates.

By the end of the code ran in the file, everything that should be passed to templates should be in the `$harmony` variable.

Again, refer to the **harmonize_example** submodule for a clean example of how to get this working for your theme!

### Customizing Harmonized Fields
Out of the box, when harmonizing Entities, the module will harmonize every available field for a given Entity. In some cases, you want to avoid harmonization certain fields.

Further customization is possible. In the `/admin/config/harmonizer/configuration` page, you can customize which fields are harmonized for every given entity type and even every bundle.

This allows you to disable harmonization on certain fields, which will omit them from the output altogether.

### Harmonizer Rest API
***UNDER CONSTRUCTION***
