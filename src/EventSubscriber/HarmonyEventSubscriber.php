<?php

namespace Drupal\harmonize\EventSubscriber;

use Drupal\Core\Theme\ThemeManager;
use Drupal\harmonize\Constants\HarmonizerFlags;
use Drupal\harmonize\Event\HarmonizationEvent;
use Drupal\harmonize\Constants\HarmonizationEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to events to apply Harmonize Strategy of harmonization data.
 *
 * @package Drupal\custom_events\EventSubscriber
 */
class HarmonyEventSubscriber implements EventSubscriberInterface {

  /**
   * Define the path that will be crawled in the theme for Handshake files.
   *
   * This is the path relative to the root of the Drupal theme.
   *
   * @var string
   */
  public const ROOT = '/harmony';

  /**
   * Define the path that will be crawled in the theme for Handshake files.
   *
   * This is the path relative to the root of the Drupal theme.
   *
   * @var string
   */
  public const FILE_EXTENSION = '.harmony.php';

  /**
   * Houses the path to the handshake directory for the current theme.
   *
   * @var string
   */
  private $directory;

  /**
   * HarmonyEventSubscriber constructor.
   *
   * @param \Drupal\Core\Theme\ThemeManager $themeManager
   *   Theme Manager injected through DI.
   */
  public function __construct(ThemeManager $themeManager) {
    $this->directory = drupal_get_path('theme', $themeManager->getActiveTheme()->getName()) . self::ROOT;
    if (file_exists($this->directory . '/functions.php')) {
      /* @noinspection PhpIncludeInspection */
      include_once $this->directory . '/functions.php';
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HarmonizationEvents::CORE => ['onHarmonize', 100],
    ];
  }

  /**
   * React to a Harmonizer event.
   *
   * @param \Drupal\harmonize\Event\HarmonizationEvent $event
   *   Event that occurred, containing the harmonizer that fired the event.
   */
  public function onHarmonize(HarmonizationEvent $event) : void {
    $harmony = [];
    $consignment = $event->harmonizer->getData();

    /* @see \Drupal\harmonize\Harmonizer\Harmonizer::getSignatureParts() */
    $signatureParts = $event->harmonizer->getSignatureParts(TRUE);

    // With each part of the signature, include files matching the part.
    foreach ($signatureParts as $part) {
      // If we have a style set, we want to find plugins of that style only.
      if ($event->harmonizer->hasFlag(HarmonizerFlags::STYLE)) {
        $part = $part . '--' . $event->harmonizer->getFlag(HarmonizerFlags::STYLE);
      }

      $this->includeAll(
        $this->directory, $part, $consignment, $harmony
      );
    }

    // If the harmony object is empty, that means it wasn't reformatted.
    // We'll return the original consignment.
    if (!empty($harmony)) {
      $event->harmonizer->setData($harmony);
    }
    else {
      $event->harmonizer->setData($consignment);
    }
  }

  /**
   * Scan the api path, recursively including all PHP files.
   *
   * @param string $dir
   *   Directory to look through to include files.
   * @param string $filename
   *   Filename to look for recursively.
   * @param mixed $consignment
   *   Data from the harmonizer, already processed by default.
   * @param mixed $harmony
   *   Data of the harmonizer that will be released to templates.
   */
  private function includeAll(string $dir, string $filename, &$consignment, &$harmony) : void {
    $escaped_filename = str_replace('.', '\.', $filename);
    $escaped_file_extension = str_replace(
      '.', '\.', self::FILE_EXTENSION
    );

    $scan = glob("{$dir}/*");
    foreach ($scan as $path) {
      $test = '/\/' . $escaped_filename . $escaped_file_extension . '$/';
      if (preg_match($test, $path)) {
        /* @noinspection PhpIncludeInspection */
        include $path;
      }
      elseif (is_dir($path)) {
        $this->includeAll($path, $filename, $consignment, $harmony);
      }
    }
  }

}
