<?php

namespace Drupal\harmonize\EventSubscriber;

use Drupal\harmonize\Constants\HarmonizerFlags;
use Drupal\harmonize\Event\HarmonizationEvent;
use Drupal\harmonize\Constants\HarmonizationEvents;
use Drupal\harmonize\PluginManager\Refinery\RefinerInterface;
use Drupal\harmonize\PluginManager\Refinery\Refinery;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes events to apply Plugin Alterations of harmonization data.
 *
 * @property Refinery $refinery
 *
 * @package Drupal\custom_events\EventSubscriber
 */
class RefineryEventSubscriber implements EventSubscriberInterface {

  /**
   * Use DI to inject the Refinery Plugin Manager.
   *
   * @var \Drupal\harmonize\PluginManager\Refinery\Refinery
   */
  private $refinery;

  /**
   * RefineryEventSubscriber constructor.
   *
   * @param \Drupal\harmonize\PluginManager\Refinery\Refinery $refinery
   *   Refinery Plugin Manager injected through DI.
   */
  public function __construct(Refinery $refinery) {
    $this->refinery = $refinery;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HarmonizationEvents::CORE => ['onHarmonize', 200],
    ];
  }

  /**
   * React to a Harmonizer event.
   *
   * @param \Drupal\harmonize\Event\HarmonizationEvent $event
   *   Event that occurred, containing the harmonizer that fired the event.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function onHarmonize(HarmonizationEvent $event) : void {
    /* @see \Drupal\harmonize\Harmonizer\Harmonizer::getSignatureParts() */
    $signatureParts = $event->harmonizer->getSignatureParts(TRUE);

    // With each part of the signature, search for Refiners matching the part.
    foreach ($signatureParts as $part) {

      // If we have a style set, we want to find plugins of that style only.
      if ($event->harmonizer->hasFlag(HarmonizerFlags::STYLE)) {
        $part = $part . '--' . $event->harmonizer->getFlag(HarmonizerFlags::STYLE);
      }

      // The signature part should match the target attribute of the Refiner.
      $refinerTarget = $part;

      // Get all plugin definitions with the specified target signature.
      $refiners = $this->refinery->getRefinersForActiveTheme($refinerTarget);

      // If there are no refiners, we can stop here.
      if (empty($refiners)) {
        continue;
      }

      // Sort the refiners by weight.
      uasort($refiners, 'self::sortRefiners');

      // Load all of the plugins found that match, and apply them to data.
      /* @var \Drupal\harmonize\PluginManager\Refinery\RefinerInterface $refiner */
      foreach ($refiners as $refiner) {
        $consignment = $event->harmonizer->getData();
        if ($refiner !== NULL) {
          $harmony = [];
          $refiner->refine($consignment, $harmony, $event->harmonizer->getObject());
          // If the harmony object is empty, that means it wasn't reformatted.
          // We'll return the original consignment.
          if (!empty($harmony)) {
            $event->harmonizer->setData($harmony);
          }
          else {
            $event->harmonizer->setData($consignment);
          }
        }
      }
    }
  }

  /* @noinspection PhpUnusedPrivateMethodInspection */

  /**
   * Sort preprocessors according to their weight.
   *
   * @param \Drupal\harmonize\PluginManager\Refinery\RefinerInterface $a
   *   First refiner plugin.
   * @param \Drupal\harmonize\PluginManager\Refinery\RefinerInterface $b
   *   Second refiner plugin.
   *
   * @return int
   *   Returns a value less than, equal to or greater than zero depending on if
   *   the first argument is considered to be respectively less than, equal to
   *   or greater than the second.
   */
  private function sortRefiners(RefinerInterface $a, RefinerInterface $b): int {
    return $a->getWeight() - $b->getWeight();
  }

}
