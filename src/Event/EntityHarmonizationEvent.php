<?php

namespace Drupal\harmonize\Event;

/**
 * Harmonization Event for Entities.
 *
 * @property \Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityHarmonizer $harmonizer
 *
 * @package Drupal\harmonize\Event
 */
class EntityHarmonizationEvent extends HarmonizationEvent {}
