<?php

namespace Drupal\harmonize\Event;

/**
 * Harmonization Event for Menus.
 *
 * @property \Drupal\harmonize\Harmonizer\MenuHarmonizer\MenuHarmonizer $harmonizer
 *
 * @package Drupal\harmonize\Event
 */
class MenuHarmonizationEvent extends HarmonizationEvent {}
