<?php

namespace Drupal\harmonize\Event;

/**
 * Harmonization Event for Forms.
 *
 * @property \Drupal\harmonize\Harmonizer\FormHarmonizer\FormHarmonizer $harmonizer
 *
 * @package Drupal\harmonize\Event
 */
class FormHarmonizationEvent extends HarmonizationEvent {}
