<?php

namespace Drupal\harmonize\Event;

use Drupal\harmonize\Harmonizer\HarmonizerInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Provides an Event for when Entities are harmonized.
 *
 * @package Drupal\harmonize\Event
 */
class HarmonizationEvent extends Event {
  /**
   * The harmonizer that fired the event.
   *
   * @var \Drupal\harmonize\Harmonizer\Harmonizer
   */
  public $harmonizer;

  /**
   * Constructs the object.
   *
   * @param \Drupal\harmonize\Harmonizer\HarmonizerInterface $harmonizer
   *   Harmonizer attached to the event that was triggered.
   */
  public function __construct(HarmonizerInterface $harmonizer) {
    $this->harmonizer = $harmonizer;
  }

}
