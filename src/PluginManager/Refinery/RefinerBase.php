<?php

namespace Drupal\harmonize\PluginManager\Refinery;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\harmonize\Service\Harmonize;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base class for Refiner plugins.
 *
 * @package Drupal\harmonize\Plugin
 */
abstract class RefinerBase extends PluginBase implements RefinerInterface, ContainerFactoryPluginInterface {

  /**
   * Constant for identifier for refiners that apply to all themes.
   */
  public const ALL_THEMES_VALUE = '*';

  /**
   * Constant for the id property of a Refiner definition.
   */
  public const ID = 'id';

  /**
   * Constant for the provider property of a Refiner definition.
   */
  public const PROVIDER = 'provider';

  /**
   * Constant for the target property of a Refiner definition.
   */
  public const TARGET = 'target';

  /**
   * Constant for the themes property of a Refiner definition.
   */
  public const THEMES = 'themes';

  /**
   * Constant for the weight property of a Refiner definition.
   */
  public const WEIGHT = 'weight';

  /**
   * Harmonize module's main service injected through DI.
   *
   * @var \Drupal\harmonize\Service\Harmonize
   */
  protected $harmonizeService;

  /**
   * Dependency injection create method override.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Dependency Injection container.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('harmonize')
    );
  }

  /**
   * The Refiner base constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\harmonize\Service\Harmonize $harmonizeService
   *   The harmonizer manager, injected through DI.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Harmonize $harmonizeService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->harmonizeService = $harmonizeService;
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getTarget(): string {
    return $this->pluginDefinition['target'];
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(): string {
    return $this->pluginDefinition['weight'];
  }

  /**
   * {@inheritdoc}
   */
  public function getThemes(): string {
    return $this->pluginDefinition['themes'];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function refine(&$consignment, &$harmony, $object) : void;

}
