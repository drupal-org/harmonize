<?php

namespace Drupal\harmonize\PluginManager\Refinery\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\harmonize\PluginManager\Refinery\RefinerBase as PluginBase;

/**
 * Defines a Refiner item annotation object.
 *
 * Plugin Namespace: Plugin\Harmonizer\Refiner.
 *
 * @see Drupal\harmonize\PluginManager\Refinery\Refinery
 * @see plugin_api
 *
 * @package Drupal\harmonize\Annotation
 *
 * @Annotation
 */
final class Refiner extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The target harmonizer signature of Refiner.
   *
   * @var string
   */
  protected $target;

  /**
   * The themes to refine harmonizers in.
   *
   * @var array
   */
  protected $themes;

  /**
   * The weight of this refiner.
   *
   * @var string
   */
  protected $weight = 0;

  /**
   * {@inheritdoc}
   */
  public function getTarget(): string {
    return $this->definition[PluginBase::TARGET];
  }

  /**
   * {@inheritdoc}
   */
  public function getThemes(): array {
    return $this->definition[PluginBase::THEMES];
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(): int {
    return $this->definition[PluginBase::WEIGHT] ?? 0;
  }

}
