<?php

namespace Drupal\harmonize\PluginManager\Refinery;

use Drupal\Component\Plugin\Discovery\DiscoveryInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Discovery\YamlDiscoveryDecorator;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\harmonize\PluginManager\Refinery\Annotation\Refiner as RefinerAnnotation;

/**
 * Provides a Plugin Manager for Refiners.
 *
 * @package Drupal\harmonize\PluginManager
 */
final class Refinery extends DefaultPluginManager {

  /**
   * Drupal's Theme Handler injected through DI.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Drupal's Theme Manager injected through DI.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Array of refiners to work with, stored statically.
   *
   * This variable is static to ensure that it is only stored once.
   *
   * @var array
   */
  private static $refiners;

  /**
   * Constructs an RefinerPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler to allow for theme plugin discovery.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   *   The theme manager injected through DI.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
    ThemeHandlerInterface $theme_handler,
    ThemeManagerInterface $theme_manager
  ) {
    parent::__construct(
      'Plugin/harmonize/Refiner',
      $namespaces,
      $module_handler,
      RefinerInterface::class,
      RefinerAnnotation::class
    );

    $this->alterInfo('harmonize_refiners_info');
    $this->setCacheBackend($cache_backend, 'harmonize_refiners');
    $this->themeHandler = $theme_handler;
    $this->themeManager = $theme_manager;
  }

  /**
   * Checks if there are plugin definitions for the current request.
   *
   * @return bool
   *   Returns TRUE if there are definitions, FALSE otherwise.
   */
  public function definitionsExist(): bool {
    // Set a static variable that tells us if definitions exist.
    static $definitionsExist = NULL;

    // If this variable is already set, return it.
    if ($definitionsExist !== NULL) {
      return $definitionsExist;
    }

    // Set variable and return whether they exist or not.
    $definitionsExist = !empty($this->getDefinitions());
    return $definitionsExist;
  }

  /**
   * Get Refiner definitions.
   *
   * This only obtains preprocessors for the currently active theme.
   *
   * @param string target
   *   The target harmonizer signature to get refiners for.
   *
   * @return array
   *   All refiner instances for the current theme.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getRefinersForActiveTheme(string $target): array {
    // If there are no plugin definitions, not much for us to do.
    if (!$this->definitionsExist()) {
      return [];
    }

    // Get the current theme.
    $activeTheme = $this->themeManager->getActiveTheme()->getName();

    // If the preprocessors for this hook are already set, get them.
    if (isset(self::$refiners[$activeTheme][$target])) {
      return self::$refiners[$activeTheme][$target];
    }

    // Get definitions first.
    $definitions = $this->getDefinitions();

    // Definitions found in themes should only be used in that theme.
    // @TODO - Handle parent/child theme relationships.
    foreach ($definitions as $definition) {
      // If a theme with the provider's name doesn't exist, we can stop here.
      if (!$this->themeHandler->themeExists($definition[RefinerBase::PROVIDER])) {
        continue;
      }

      // Otherwise, we check if the current theme is the definition's theme.
      if ($activeTheme !== $definition[RefinerBase::PROVIDER]) {
        // If it isn't, we remove this definition.
        unset($definitions[$definition[RefinerBase::ID]]);
      }
      else {
        // If it is, we set the definition's theme to this provider only.
        $definitions[$definition[RefinerBase::ID]][RefinerBase::THEMES] = [$definition[RefinerBase::PROVIDER]];
      }
    }

    // Format the definitions.
    foreach ($definitions as $definition) {
      // If the definition doesn't have a target set, we shouldn't return it.
      if (empty($definition[RefinerBase::TARGET])) {
        continue;
      }

      // If themes are set, and the current theme isn't in it, don't set it.
      if (!empty($definition[RefinerBase::THEMES]) && !\in_array($activeTheme, $definition[RefinerBase::THEMES], FALSE)) {
        continue;
      }

      // Set the plugin instance in the array according to all of its themes.
      self::$refiners[$activeTheme][$definition[RefinerBase::TARGET]][$definition[RefinerBase::ID]] = $this->createInstance($definition[RefinerBase::ID]);
    }

    // Return the refiners.
    return self::$refiners[$activeTheme][$target] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery(): DiscoveryInterface {
    if (!$this->discovery) {
      $discovery = new AnnotatedClassDiscovery($this->subdir, $this->namespaces, $this->pluginDefinitionAnnotationName, $this->additionalAnnotationNamespaces);
      $discovery = new YamlDiscoveryDecorator($discovery, 'refiners', array_merge($this->moduleHandler->getModuleDirectories(), $this->themeHandler->getThemeDirectories()));
      $discovery = new ContainerDerivativeDiscoveryDecorator($discovery);
      $this->discovery = $discovery;
    }

    return $this->discovery;
  }

  /**
   * {@inheritdoc}
   */
  protected function providerExists($provider): bool {
    return $this->moduleHandler->moduleExists($provider) || $this->themeHandler->themeExists($provider);
  }

}
