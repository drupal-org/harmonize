<?php

namespace Drupal\harmonize\PluginManager\Refinery;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Provides an interface for Refiner plugins.
 *
 * @package Drupal\harmonize\PluginManager\Refinery
 */
interface RefinerInterface extends PluginInspectionInterface {

  /**
   * Return the ID of the refiner.
   *
   * @return string
   *   Return the ID of the Refiner.
   */
  public function getId(): string;

  /**
   * Return the target of the Refiner.
   *
   * @return string
   *   Return the Target Signature of the Harmonizer of the Refiner.
   */
  public function getTarget(): string;

  /**
   * Return the weight of the Refiner.
   *
   * @return string
   *   Return the weight of the Refiner.
   */
  public function getWeight(): string;

  /**
   * Return the themes this refiner will act in.
   *
   * @return string
   *   Return the themes.
   */
  public function getThemes(): string;

  /**
   * Refine the data provided by the base harmonizer.
   *
   * @param mixed $consignment
   *   The data initially altered by the base harmonizer.
   * @param mixed $harmony
   *   An empty object that can optionally be used to house the data that will
   *   be passed on to the next stage of harmonization. If this object is empty,
   *   the consignment will be passed on instead.
   * @param mixed $object
   *   Original object that the Harmonizer used to build the data.
   */
  public function refine(&$consignment, &$harmony, $object) : void;

}
