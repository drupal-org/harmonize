<?php

namespace Drupal\harmonize\Plugin\Hook;

use Drupal\Core\Cache\Cache;
use Drupal\harmonize\Constants\HarmonizerTypes;
use Drupal\crocheteer\Hook\Entity\PreSave\HookEntityPreSavePlugin;

/**
 * Class HookEntityPreSave.
 *
 * Only implement the ContainerFactoryPluginInterface if you need dependency
 * injection mechanisms.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @HookEntityPreSave(
 *   id = "harmonizer_entity_pre_save",
 *   title = @Translation("Harmonizer - Entity Pre-Save Hook"),
 *   entityTypeIds = {
 *     "node",
 *     "paragraph",
 *   },
 *   bundles = {},
 * )
 */
final class HookEntityPreSaveNode extends HookEntityPreSavePlugin {

  /**
   * {@inheritdoc}
   */
  public function hook() : void {
    // Get the entity being saved.
    $entity = $this->event->getEntity();
    Cache::invalidateTags([HarmonizerTypes::ENTITY . '.' . $entity->getEntityTypeId() . '.' . $entity->bundle() . '.' . $entity->id()]);
  }

}
