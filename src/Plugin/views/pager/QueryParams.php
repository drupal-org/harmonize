<?php

namespace Drupal\harmonize\Plugin\views\pager;

use Drupal\views\Plugin\views\pager\Some;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Plugin for views without pagers.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *   LongInheritanceChainInspection
 *
 * @ingroup views_pager_plugins
 *
 * @ViewsPager(
 *   id = "query_params",
 *   title = @Translation("Query parameters"),
 *   help = @Translation("Query parameters"),
 *   display_types = {"data"}
 * )
 */
class QueryParams extends Some {

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, RequestStack $requestStack) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->currentRequest = $requestStack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    parent::query();

    if ($limit = $this->currentRequest->get('limit', FALSE)) {
      $this->view->query->setLimit(\intval($limit));
    }

    if ($offset = $this->currentRequest->get('offset', FALSE)) {
      $this->view->query->setOffset(\intval($offset));
    }

  }

}
