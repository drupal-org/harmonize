<?php

namespace Drupal\harmonize\Plugin\preprocessors;

/**
 *  * Provide plugin to preprocess variables for paragraphs.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Preprocessor(
 *   id = "harmonize.paragraph_preprocessor",
 *   theme_hook = "paragraph"
 * )
 *
 * @package Drupal\harmonize\Plugin\Preprocessor
 */
final class ParagraphPreprocessor extends HarmonizePreprocessorBase {

  /**
   * Add personal tweaks to variables in this function.
   *
   * {@inheritdoc}
   */
  public function preprocess(&$variables, $hook, $info) : void {
    $variables['data'] = $this->harmonizeService->autoHarmonize($variables['paragraph']);
  }

}
