<?php

namespace Drupal\harmonize\Plugin\preprocessors;

/**
 *  * Provide plugin to preprocess variables for menus.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Preprocessor(
 *   id = "harmonize.menu_preprocessor",
 *   theme_hook = "menu"
 * )
 *
 * @package Drupal\harmonize\Plugin\Preprocessor
 */
final class MenuPreprocessor extends HarmonizePreprocessorBase {

  /**
   * Add personal tweaks to variables in this function.
   *
   * {@inheritdoc}
   */
  public function preprocess(&$variables, $hook, $info) : void {
    $variables['data'] = $this->harmonizeService->autoHarmonize($variables);
  }

}
