<?php

namespace Drupal\harmonize\Plugin\preprocessors;

use Drupal\harmonize\Service\Harmonize;
use Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\PreprocessorPluginBase;
use Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\PreprocessorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/* @noinspection AnnotationMissingUseInspection */
/**
 * Provide plugin to alter harmonizer data for all Regions.
 *
 * @Preprocessor(
 *   id = "harmonize_paragraph_preprocess",
 *   template = "paragraph"
 * )
 *
 * @package Drupal\harmonize\Plugin\Preprocessor
 */
final class ParagraphPreprocess extends PreprocessorPluginBase {

  /**
   * The Harmonize Service injected through DI.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $harmonizeService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) : PreprocessorInterface {
    /* @noinspection PhpParamsInspection */
    return new static($configuration, $pluginId, $pluginDefinition, $container->get('harmonize'));
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Harmonize $harmonizeService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->harmonizeService = $harmonizeService;
  }

  /**
   * Add personal tweaks to variables in this function.
   *
   * {@inheritdoc}
   */
  public function preprocess(&$variables, $hook, $info) : void {
    $variables['data'] = $this->harmonizeService->autoHarmonize($variables['paragraph']);
  }

}
