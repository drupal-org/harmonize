<?php

namespace Drupal\harmonize\Plugin\preprocessors;

use Drupal\harmonize\Service\Harmonize;
use Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\PreprocessorPluginBase;
use Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\PreprocessorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide a base class for preprocessors in the Harmonize module.
 *
 * @package Drupal\harmonize\Plugin\Preprocessor
 */
abstract class HarmonizePreprocessorBase extends PreprocessorPluginBase {

  /**
   * The Harmonize Service injected through DI.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $harmonizeService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) : PreprocessorInterface {
    /* @noinspection PhpParamsInspection */
    return new static($configuration, $pluginId, $pluginDefinition, $container->get('harmonize'));
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Harmonize $harmonizeService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->harmonizeService = $harmonizeService;
  }

}
