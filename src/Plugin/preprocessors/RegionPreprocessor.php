<?php

namespace Drupal\harmonize\Plugin\preprocessors;

/**
 * Provide plugin to preprocess variables for all regions.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Preprocessor(
 *   id = "harmonize.region_preprocess",
 *   theme_hook = "region"
 * )
 *
 * @package Drupal\harmonize\Plugin\Preprocessor
 */
final class RegionPreprocessor extends HarmonizePreprocessorBase {

  /**
   * Add personal tweaks to variables in this function.
   *
   * {@inheritdoc}
   */
  public function preprocess(&$variables, $hook, $info) : void {
    $variables['data'] = $this->harmonizeService->autoHarmonize($variables);
  }

}
