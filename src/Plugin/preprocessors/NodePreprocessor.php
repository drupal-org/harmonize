<?php

namespace Drupal\harmonize\Plugin\preprocessors;

/**
 * Provide plugin to preprocess variables for all nodes.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Preprocessor(
 *   id = "harmonize.node_preprocessor",
 *   theme_hook = "node"
 * )
 *
 * @package Drupal\harmonize\Plugin\Preprocessor
 */
final class NodePreprocessor extends HarmonizePreprocessorBase {

  /**
   * Add personal tweaks to variables in this function.
   *
   * {@inheritdoc}
   */
  public function preprocess(&$variables, $hook, $info) : void {
    $variables['data'] = $this->harmonizeService->autoHarmonize($variables['node']);
  }

}
