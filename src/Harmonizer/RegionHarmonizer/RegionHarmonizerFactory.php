<?php

namespace Drupal\harmonize\Harmonizer\RegionHarmonizer;

use Drupal\harmonize\Harmonizer\HarmonizerFactory;
use Drupal\harmonize\Constants\HarmonizerTypes;

/**
 * Manages which RegionHarmonizer class should be used to preprocess the region.
 *
 * There is only one type for now, but this factory makes it
 * easy to extend for future region types that might appear in the future.
 *
 * @package Drupal\harmonize\Harmonizer\MenuHarmonizer
 */
class RegionHarmonizerFactory extends HarmonizerFactory {

  /**
   * {@inheritdoc}
   */
  protected static function type() : string {
    return HarmonizerTypes::REGION;
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveSignature($object) : string {
    // Generate a signature for the harmonizer about to be instantiated.
    // Example: menu.main_navigation.
    $signature = self::generateHarmonizerSignature(HarmonizerTypes::REGION, $object['region']);
    return $signature;
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveClass($object): string {
    return RegionHarmonizer::class;
  }

}
