<?php

namespace Drupal\harmonize\Harmonizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer\EntityFieldHarmonizerFactory;
use Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityHarmonizerFactory;
use Drupal\harmonize\Harmonizer\FormHarmonizer\FormHarmonizerFactory;
use Drupal\harmonize\Harmonizer\MenuHarmonizer\MenuHarmonizerFactory;
use Drupal\harmonize\Harmonizer\RegionHarmonizer\RegionHarmonizerFactory;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\harmonize\Service\Harmonize;

/**
 * Handles the creation of the proper harmonizer for a given Drupal object.
 *
 * There are many types of objects in Drupal. Each
 * object may have their own specific arrangements that will affect the way the
 * data is pre-processed. We will handle these in respective classes.
 *
 * By checking the object type, the Factory builds the appropriate Harmonizer.
 *
 * @package Drupal\harmonize\Harmonizer
 */
final class MasterHarmonizerFactory {

  /**
   * Build the appropriate Harmonizer with the given object.
   *
   * This Master Factory calls appropriate sub-factories.
   *
   * @param mixed $object
   *   Drupal Object that is currently being processed.
   * @param array $flags
   *   Flags that affect the actions taken when harmonizing the object.
   * @param \Drupal\harmonize\Service\Harmonize $harmonizeService
   *   Harmonize module's main service service containing many Drupal services.
   *
   * @return mixed|null
   *   Returns the proper Harmonizer. If it fails, returns null.
   */
  public static function build($object, array $flags, Harmonize $harmonizeService) {

    // If for some reason we try to harmonize NULL, let's get out of here.
    if ($object === NULL) {
      // Throw status message saying tried to harmonize a null object.
      return NULL;
    }

    // First, check if the object is an Entity.
    if (\is_object($object) && \in_array(ContentEntityInterface::class, class_implements($object), TRUE)) {
      return EntityHarmonizerFactory::build($object, $flags, $harmonizeService);
    }

    // Next, we'll check if it's a field.
    if (\is_object($object) && \in_array(FieldItemListInterface::class, class_implements($object), TRUE)) {
      return EntityFieldHarmonizerFactory::build($object, $flags, $harmonizeService);
    }

    // Check if the object is a render array.
    if (\is_array($object)) {
      // Check if the object is a form render array.
      if (isset($object['#type']) && $object['#type'] === 'form') {
        return FormHarmonizerFactory::build($object, $flags, $harmonizeService);
      }
      // Check if this object is a menu render array.
      if ((isset($object['menu_name']) && !empty($object['items'])) || (isset($object['#menu_name']) && !empty($object['#items']))) {
        return MenuHarmonizerFactory::build($object, $flags, $harmonizeService);
      }
      // Check if this object is a region render array.
      if (isset($object['region'])) {
        return RegionHarmonizerFactory::build($object, $flags, $harmonizeService);
      }
    }

    // Throw a Drupal log error if no Harmonizer was found for the given object.
    // Throw a status message too!
    // @TODO - USE DI!
    \Drupal::logger('harmonizer')->notice(
      t(
        'Error: Unhandled Harmonizer. An object was trying to be preprocessed, but no Harmonizer class was found. Please check the backend code for details.'
      )
    );

    return NULL;
  }

}
