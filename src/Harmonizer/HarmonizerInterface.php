<?php

namespace Drupal\harmonize\Harmonizer;

/**
 * Provides an interface for Harmonizers.
 *
 * @package Drupal\harmonize\Harmonizer
 */
interface HarmonizerInterface {

  /**
   * Preprocess data.
   *
   * @return mixed
   *   Return the harmonizer's pre-processed data.
   */
  public function harmonize();

  /**
   * Generate Cache Tags for this harmonizer.
   *
   * Each type of Harmonizer should generate it's own cache tags. This affects
   * how cashing is managed for the harmonizers.
   */
  public function getCacheTags(): array;

  /**
   * Return whether or not the result of this harmonizer should be cached.
   *
   * @return bool
   *   Returns TRUE if the result should be cached. FALSE otherwise.
   */
  public function isCacheable(): bool;

  /**
   * Return whether or not caching for this harmonizer is enabled.
   *
   * @return bool
   *   Returns TRUE if caching is enabled FALSE otherwise.
   */
  public function cacheEnabled(): bool;

  /**
   * Get Data field of the harmonizer.
   *
   * @return mixed
   *   Return the harmonizer's data.
   */
  public function getData();

  /**
   * Set Data field of the harmonizer.
   *
   * @param mixed $data
   *   Set the harmonizer's data to a given value.
   */
  public function setData($data): void;

  /**
   * Get the object property of the current harmonizer.
   *
   * @return mixed
   *   Return the object of the current harmonizer.
   */
  public function getObject();

  /**
   * Get the type of the harmonizer.
   *
   * @return string
   *   Return the type of harmonizer.
   */
  public function getType(): string;

  /**
   * Get the value of a flag set to the harmonizer.
   *
   * Flags can be used to customize the actions of a harmonizer.
   *
   * @param string $key
   *   Key of the flag value to look fetch.
   *
   * @return null|string
   *   Returns value of the requested flag.
   */
  public function getFlag(string $key): ?string;

  /**
   * Check if the current harmonizer has a flag set.
   *
   * Flags can be used to customize the actions of a harmonizer.
   *
   * @param string $key
   *   Key of the flag to look for.
   *
   * @return bool
   *   Returns true if the flag is planted. Returns false otherwise.
   */
  public function hasFlag(string $key): bool;

  /**
   * Set a flag to the current harmonizer.
   *
   * Flags can be used to customize the actions of a harmonizer.
   *
   * @param string $key
   *   Key of the flag to be set.
   * @param string $value
   *   Value of the flag to be set.
   */
  public function setFlag(string $key, string $value): void;

  /**
   * Set flags to the current harmonizer.
   *
   * Flags can be used to customize the actions of a harmonizer.
   *
   * @param array $flags
   *   Array of flags to set on this harmonizer.
   */
  public function setFlags(array $flags): void;

  /**
   * Retrieve signature from this Harmonizer.
   *
   * @return string
   *   Signature in string format.
   */
  public function getSignature() : string;

  /**
   * Set signature for this harmonizer.
   *
   * @param string $signature
   *   String to set the signature to.
   */
  public function setSignature(string $signature): void;

  /**
   * Get the signature property of the current harmonizer.
   *
   * If incremental parts are requested, an array will be returned containing
   * each part of the harmonizer's signature incrementally appended.
   *
   * Example: A signature signed "entity.node.basic_page" will return an array
   * with the following values:
   * ['entity', 'entity.node', 'entity.node.basic_page'].
   *
   * The incremental signature array form is used in many of the module's
   * features.
   *
   * @param bool $incremental_parts
   *   Determines whether to get the signature in incremental parts. This will
   *   return the signature in an array instead of a string, and it will be
   *   split in the proper parts according to the rule above.
   *
   * @return mixed
   *   Return the signature of the current harmonizer.
   *   Return array if parts are requested.
   */
  public function getSignatureParts(bool $incremental_parts = FALSE);

  /**
   * Get the parent harmonizer for this harmonizer.
   *
   * @return \Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityHarmonizer
   *   Return the parent harmonizer class linked to this harmonizer, if any.
   */
  public function getParentHarmonizer() : ?Harmonizer;

  /**
   * Set Parent Harmonizer of this Harmonizer.
   *
   * @param \Drupal\harmonize\Harmonizer\Harmonizer $parentHarmonizer
   *   The parent harmonizer class.
   */
  public function setParentHarmonizer(Harmonizer $parentHarmonizer);

}
