<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

/**
 * Handles harmonization exceptions for 'metatag' type fields.
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class MetaTagEntityFieldHarmonizer extends EntityFieldHarmonizer {

  /**
   * {@inheritdoc}
   */
  public function process(array $value, int $i) {
    return $value;
  }

}
