<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

/**
 * Handles exceptions for 'datetime' type fields.
 *
 * @property \Drupal\harmonize\Service\Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class DateRangeEntityFieldHarmonizer extends EntityFieldHarmonizer {

  /**
   * {@inheritDoc}
   */
  public function process(array $value, int $i) {
    return [
      'from' => $this->timeZoneShift($value['value']),
      'to' => $this->timeZoneShift($value['end_value']),
    ];
  }

  /**
   * Make sure that the timezone is correctly set.
   *
   * @param string $date
   *   The date saved in the database.
   *
   * @return \DateTime|string
   *   The date with the correct time zone.
   */
  private function timeZoneShift($date) {
    try {
      $utcTimeZone = new \DateTimeZone('UTC');
      $currentTimeZone = new \DateTimeZone(date_default_timezone_get());
      $date = new \DateTime($date, $utcTimeZone);

      return $date->setTimezone($currentTimeZone)->format('c');
    }
    catch (\Exception $e) {
      return $date;
    }
  }

}
