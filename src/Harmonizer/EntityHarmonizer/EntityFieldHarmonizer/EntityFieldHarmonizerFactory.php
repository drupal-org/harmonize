<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

use Drupal\harmonize\Constants\HarmonizerTypes;
use Drupal\harmonize\Harmonizer\HarmonizerFactory;

/**
 * Handles the creation of the proper field harmonizer with a given Field.
 *
 * There are many types of Fields in Drupal. Each
 * Field will have their own specific arrangements that will affect the way the
 * data is pre-processed. We will handle these in respective classes.
 *
 * By checking the field type, the Factory builds the appropriate
 * EntityFieldHarmonizer.
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
final class EntityFieldHarmonizerFactory extends HarmonizerFactory {

  /**
   * {@inheritdoc}
   */
  protected static function type() : string {
    return HarmonizerTypes::ENTITY_FIELD;
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveSignature($object) : string {
    // Generate a signature for the harmonizer about to be instantiated.
    // Example: field.email.field_contact_email.
    $signature = self::generateHarmonizerSignature(
      HarmonizerTypes::ENTITY_FIELD,
      $object->getFieldDefinition()->getType(),
      $object->getName(),
      $object->getFieldDefinition()->getTargetEntityTypeId(),
      $object->getFieldDefinition()->getTargetBundle(),
      $object->getEntity()->id()
    );
    return $signature;
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveClass($object): string {
    // Otherwise, we instantiate it and register it in the factory.
    // Get the type of field.
    $fieldType = $object->getFieldDefinition()->getType();

    // Initialize variable to hold class.
    $class = NULL;

    // Depending on the type, create the appropriate HarmonizerInterface.
    switch ($fieldType) {

      case 'file':
        $class = FileEntityFieldHarmonizer::class;
        break;

      case 'boolean':
        $class = BooleanEntityFieldHarmonizer::class;
        break;

      case 'daterange':
        $class = DateRangeEntityFieldHarmonizer::class;
        break;

      case 'datetime':
        $class = DateTimeEntityFieldHarmonizer::class;
        break;

      case 'image':
        $class = ImageEntityFieldHarmonizer::class;
        break;

      case 'metatag':
        $class = MetaTagEntityFieldHarmonizer::class;
        break;

      case 'link':
        $class = LinkEntityFieldHarmonizer::class;
        break;

      case 'entity_reference':
        $class = EntityReferenceEntityFieldHarmonizer::class;
        break;

      case 'entity_reference_revisions':
        $class = EntityReferenceRevisionsEntityFieldHarmonizer::class;
        break;

      case 'text_with_summary':
        $class = TextWithSummaryEntityFieldHarmonizer::class;
        break;

      case 'color_field_type':
        $class = ColorEntityFieldHarmonizer::class;
        break;

      default:
        $class = EntityFieldHarmonizer::class;
        break;
    }

    return $class;
  }

}
