<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

/**
 * Handles harmonization exceptions for 'color_field_type' type fields.
 *
 * @property \Drupal\harmonize\Service\Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class ColorEntityFieldHarmonizer extends EntityFieldHarmonizer {

  /**
   * {@inheritdoc}
   */
  public function process(array $value, int $i) {
    return $value['color'];
  }

}
