<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

use Drupal\harmonize\Constants\HarmonizeConfig;
use Drupal\harmonize\Constants\HarmonizerFlags;
use Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityHarmonizerFactory;
use Symfony\Component\Yaml\Yaml;

/**
 * Handles exceptions for 'entity_reference' type fields.
 *
 * @property \Drupal\harmonize\Service\Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class EntityReferenceEntityFieldHarmonizer extends EntityFieldHarmonizer {

  /**
   * {@inheritdoc}
   */
  public function process(array $value, int $i) {
    // Get the entity from the field value.
    $entity = $this->getFieldData()[$i]->entity;

    if ($entity === NULL) {
      return NULL;
    }

    // Get the translated entity.
    $entity = $this->harmonizeService->helpers->ensureTranslated($entity);

    // Build harmonizer for this entity.
    $flags = [
      HarmonizerFlags::PARENT => $this->getParentHarmonizer(),
    ];
    // If we have any flags here, the harmonizer must inherit some of them.
    if (!empty($this->flags)) {
      $flags[HarmonizerFlags::LANG] = $this->flags[HarmonizerFlags::LANG] ?? '';
    }
    $harmonizer = EntityHarmonizerFactory::build($entity, $flags, $this->harmonizeService);

    // If we're referencing nodes or paragraphs, we do a depth check.
    // This depth check should be ignored if we are harmonizing medias or terms.
    // This is because medias and terms don't really mind being re-harmonized.
    // We also want to check if there's a harmonize style set.
    // If there is, we want full control over what is harmonized and what isn't.
    // This is to save some load time as well.
    $bundle = $entity->getEntityTypeId();
    if ($bundle === 'node' || $bundle === 'paragraph') {
      if (
        isset($this->flags[HarmonizerFlags::STYLE])
        || (isset($this->flags[HarmonizerFlags::NO_RECURSIVE]) && $this->flags[HarmonizerFlags::NO_RECURSIVE])
        || $this->depth() > $this->harmonizeService->config[HarmonizeConfig::MAX_RECURSIVE_DEPTH]
      ) {
        return $harmonizer;
      }
    }

    // Now we want to check if we need to set a style.
    $style = $this->getConfiguredStyleFromTheme();
    if ($style !== NULL) {
      $harmonizer->setFlag(HarmonizerFlags::STYLE, $style);
    }

    // Return harmonized data.
    return $harmonizer->harmonize();
  }

  /**
   * Does things.
   */
  public function getConfiguredStyleFromTheme(): ?string {
    // Cache.
    $cid = 'harmonizer::harmony_config';
    $cachedConfig = $this->harmonizeService->cacheBackend->get($cid);

    if (!$cachedConfig) {
      // We want to check if a Harmony declaration file exists in the
      // site's main theme. If one does, we want to read it and see if a
      // declaration exists for this particular context to set styles.
      $themeDirectory = drupal_get_path('theme', \Drupal::theme()->getActiveTheme()->getName());
      $harmonyDeclarationPath = $themeDirectory . '/' . \Drupal::theme()->getActiveTheme()->getName() . '.harmony.yml';

      // If no declaration file exists, stop.
      if (!file_exists($harmonyDeclarationPath)) {
        return NULL;
      }

      // Get the configuration file.
      $harmonyConfig = Yaml::parse(file_get_contents($harmonyDeclarationPath));

      // Store it in the cache.
      $this->harmonizeService->cacheBackend->set('', $harmonyConfig);
    }
    else {
      $harmonyConfig = $cachedConfig;
    }

    // If no entity field styles configurations exist, stop.
    if (!isset($harmonyConfig['entity_field_styles'])) {
      return NULL;
    }

    // Get the parent entity type.
    $parentEntityType = $this->fieldDefinition->getTargetEntityTypeId();

    if (!isset($harmonyConfig['entity_field_styles'][$parentEntityType])) {
      return NULL;
    }

    // Get parent entity bundle.
    $parentEntityBundle = $this->fieldDefinition->getTargetBundle();

    if (!isset($harmonyConfig['entity_field_styles'][$parentEntityType][$parentEntityBundle])) {
      return NULL;
    }

    if (!isset($harmonyConfig['entity_field_styles'][$parentEntityType][$parentEntityBundle][$this->fieldName])) {
      return NULL;
    }

    if (!isset($harmonyConfig['entity_field_styles'][$parentEntityType][$parentEntityBundle][$this->fieldName]['style'])) {
      return NULL;
    }

    return $harmonyConfig['entity_field_styles'][$parentEntityType][$parentEntityBundle][$this->fieldName]['style'];

  }

}
