<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

use Drupal\Core\Url;

/**
 * Handles harmonization exceptions for 'link' type fields.
 *
 * @property \Drupal\harmonize\Service\Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class LinkEntityFieldHarmonizer extends EntityFieldHarmonizer {

  /**
   * {@inheritdoc}
   */
  public function process(array $value, int $i) {
    $label = $value['title'] ?? '';
    $attributes = [
      'href'   => Url::fromUri($value['uri'])->toString(),
      'target' => $value['options']['attributes']['target'] ?? '_self',
    ];

    return $this->harmonizeService->helpers->buildLink($label, $attributes);
  }

}
