<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

use Drupal\harmonize\Constants\HarmonizerFlags;
use Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityHarmonizerFactory;

/**
 * Handles exceptions for 'file' type fields.
 *
 * @property \Drupal\harmonize\Service\Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class FileEntityFieldHarmonizer extends EntityFieldHarmonizer {

  /**
   * {@inheritdoc}
   */
  public function process(array $value, int $i) {

    // Get the entity from the field value.
    $entity = $this->getFieldData()[$i]->entity;

    if ($entity === NULL) {
      return NULL;
    }

    // Get the translated entity.
    $entity = $this->harmonizeService->helpers->ensureTranslated($entity);

    // Build harmonizer for this entity.
    $flags = [
      HarmonizerFlags::PARENT => $this->getParentHarmonizer(),
    ];
    // If we have any flags here, the harmonizer must inherit some of them.
    if (!empty($this->flags)) {
      $flags[HarmonizerFlags::LANG] = $this->flags[HarmonizerFlags::LANG] ?? '';
    }
    $harmonizer = EntityHarmonizerFactory::build($entity, $flags, $this->harmonizeService);

    // Return harmonized data.
    return $harmonizer->harmonize();
  }

}
