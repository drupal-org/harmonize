<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

use Drupal\harmonize\Harmonizer\HarmonizerInterface;

/**
 * Provides an interface for Entity Field Harmonizers.
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer
 */
interface EntityFieldHarmonizerInterface extends HarmonizerInterface {

}
