<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

/**
 * Handles exceptions for 'entity_reference_revisions' type fields.
 *
 * @noinspection
 *   LongInheritanceChainInspection
 *
 * @property \Drupal\harmonize\Service\Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class EntityReferenceRevisionsEntityFieldHarmonizer extends EntityReferenceEntityFieldHarmonizer {}
