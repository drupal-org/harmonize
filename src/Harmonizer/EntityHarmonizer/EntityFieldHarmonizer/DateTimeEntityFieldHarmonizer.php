<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

/**
 * Handles exceptions for 'datetime' type fields.
 *
 * @property \Drupal\harmonize\Service\Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class DateTimeEntityFieldHarmonizer extends EntityFieldHarmonizer {}
