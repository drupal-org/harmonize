<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

/**
 * Handles exceptions for 'text_with_summary' type fields.
 *
 * @property \Drupal\harmonize\Service\Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class TextWithSummaryEntityFieldHarmonizer extends EntityFieldHarmonizer {

  /**
   * {@inheritdoc}
   */
  public function process(array $value, int $i) {
    unset($value['format'], $value['_attributes']);
    return $value;
  }

}
