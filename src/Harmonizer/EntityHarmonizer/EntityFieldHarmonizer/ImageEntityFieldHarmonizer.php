<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

/**
 * Handles exceptions for 'image' type fields.
 *
 * @property \Drupal\harmonize\Service\Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class ImageEntityFieldHarmonizer extends EntityFieldHarmonizer {

  /**
   * {@inheritdoc}
   */
  public function process(array $value, int $i) {
    // Get the entity from the field value.
    $entity = $this->getFieldData()[$i]->entity;

    if ($entity === NULL) {
      return NULL;
    }

    // Image file data, harmonized by yours truly.
    $file = $this->harmonizeService->harmonize($entity);
    $src = $file['href'];
    $alt = $this->getFieldData()->getValue()[$i]['alt'] ?? '';
    $title = $this->getFieldData()->getValue()[$i]['title'] ?? '';
    $data = [
      'uri' => $file['uri'],
    ];
    $attributes = [
      'src'   => $src,
      'alt'   => $alt,
      'title' => $title,
    ];

    return $this->harmonizeService->helpers->buildImage($data, $attributes);

  }

}
