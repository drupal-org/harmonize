<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer;

use Drupal\harmonize\Constants\HarmonizerTypes;
use Drupal\harmonize\Harmonizer\HarmonizerFactory;

/**
 * Handles the creation of the proper harmonizer for a given Entity.
 *
 * There are many types of Entities in Drupal. Each Entity may
 * have their own specific arrangements that will affect the way the data is
 * preprocessed. We will handle these in respective classes.
 *
 * By checking the entity type, the Factory builds the appropriate Harmonizer.
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityHarmonizer
 */
final class EntityHarmonizerFactory extends HarmonizerFactory {

  /**
   * {@inheritdoc}
   */
  protected static function type() : string {
    return HarmonizerTypes::ENTITY;
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveSignature($object) : string {
    // Generate a signature for the harmonizer about to be instantiated.
    // Example: entity.node.basic_page.
    /* @var \Drupal\node\Entity\Node $object */
    $signature = self::generateHarmonizerSignature(HarmonizerTypes::ENTITY, $object->getEntityTypeId(), $object->bundle(), $object->id(), $object->getLoadedRevisionId());
    return $signature;
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveClass($object): string {
    // Get the type of entity.
    // i.e. "node", "paragraph", "media", etc.
    $entity_type = $object->getEntityTypeId();

    // Depending on the type, create the appropriate "Harmonizer".
    switch ($entity_type) {
      case 'node':
        $class = NodeEntityHarmonizer::class;
        break;

      case 'file':
        $class = FileEntityHarmonizer::class;
        break;

      case 'media':
        $class = MediaEntityHarmonizer::class;
        break;

      case 'paragraph':
        $class = ParagraphEntityHarmonizer::class;
        break;

      case 'taxonomy_term':
        $class = TaxonomyTermEntityHarmonizer::class;
        break;

      default:
        $class = EntityHarmonizer::class;
        break;
    }

    return $class;
  }

}
