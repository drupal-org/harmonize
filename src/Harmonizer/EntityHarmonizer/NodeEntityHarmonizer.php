<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer;

/**
 * Handles harmonization exceptions for Node entities.
 *
 * @property \Drupal\node\Entity\Node $entity
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer
 */
final class NodeEntityHarmonizer extends EntityHarmonizer {

  /**
   * {@inheritdoc}
   */
  protected function getHarmonizedData() {
    $data = parent::getHarmonizedData();

    if (!\is_array($data)) {
      $data = ['content' => $data];
    }

    // Get Entity Title if it exists.
    if ($this->entity->getTitle() !== NULL) {
      $data['title'] = $this->entity->getTitle();
    }

    // Add URL information if node is not new.
    if (!$this->entity->isNew()) {
      $url = $this->entity->url('canonical', ['absolute' => FALSE]);
      $data['href'] = $url;
    }

    // Add other relevant information for nodes.
    $data['creation_date'] = $this->entity->getCreatedTime();

    /* @var \Drupal\user\Entity\User */
    $data['drupal_author'] = $this->harmonizeService->harmonize($this->entity->getOwner());

    return $data;
  }

}
