<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer;

/**
 * Handles harmonization exceptions for TaxonomyTerm entities.
 *
 * @property \Drupal\taxonomy\Entity\Term $entity
 *
 * @package Drupal\harmonize\Harmonizer\TaxonomyTermEntityHarmonizer
 */
final class TaxonomyTermEntityHarmonizer extends EntityHarmonizer {

  /**
   * {@inheritdoc}
   */
  protected function getHarmonizedData() {
    // Run the EntityHarmonizerBase function. No exceptions here.
    $data = parent::getHarmonizedData();

    // Add term title to the processed data.
    $data['name'] = $this->entity->getName();

    // Add term description to the processed data.
    $data['description'] = $this->entity->getDescription();

    // Add URL information if node is not new.
    if (!$this->entity->isNew()) {
      $url = $this->entity->url('canonical');
      $data['href'] = $url;
    }

    return $data;
  }

}
