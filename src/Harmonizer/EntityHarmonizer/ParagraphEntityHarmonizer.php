<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer;

/**
 * Handles harmonization exceptions for Paragraph entities.
 *
 * @property \Drupal\paragraphs\ParagraphInterface $entity
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer
 */
final class ParagraphEntityHarmonizer extends EntityHarmonizer {}
