<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer;

/**
 * Handles harmonization exceptions for Media entities.
 *
 * @property \Drupal\media\MediaInterface $entity
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer
 */
final class MediaEntityHarmonizer extends EntityHarmonizer {

  /**
   * {@inheritdoc}
   */
  protected function getHarmonizedData() {
    $data = parent::getHarmonizedData();

    // Add media title to the processed data.
    $data['media_title'] = $this->entity->get('name')->getValue()[0]['value'];

    return $data;
  }

}
