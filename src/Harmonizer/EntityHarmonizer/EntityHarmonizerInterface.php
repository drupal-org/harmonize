<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer;

use Drupal\harmonize\Harmonizer\HarmonizerInterface;

/**
 * Provides an interface for EntityHarmonizers.
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer
 */
interface EntityHarmonizerInterface extends HarmonizerInterface {

}
