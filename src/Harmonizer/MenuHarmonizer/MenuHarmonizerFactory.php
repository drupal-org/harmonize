<?php

namespace Drupal\harmonize\Harmonizer\MenuHarmonizer;

use Drupal\harmonize\Harmonizer\HarmonizerFactory;
use Drupal\harmonize\Constants\HarmonizerTypes;

/**
 * Manages which MenuHarmonizer class should be used to pre-process the menu.
 *
 * There is only one type for now, but this factory makes it
 * easy to extend for future menu types or different types of menu arrays.
 *
 * @package Drupal\harmonize\Harmonizer\MenuHarmonizer
 */
class MenuHarmonizerFactory extends HarmonizerFactory {

  /**
   * {@inheritdoc}
   */
  protected static function type() : string {
    return HarmonizerTypes::MENU;
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveSignature($object) : string {
    // Generate a signature for the harmonizer about to be instantiated.
    // Example: menu.main_navigation.
    $menuName = $object['menu_name'] ?? $object['#menu_name'];
    $signature = self::generateHarmonizerSignature(HarmonizerTypes::MENU, $menuName);
    return $signature;
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveClass($object): string {
    return MenuHarmonizer::class;
  }

}
