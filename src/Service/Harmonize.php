<?php

namespace Drupal\harmonize\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Theme\ThemeManager;
use Drupal\harmonize\Constants\HarmonizeConfig;
use Drupal\harmonize\Harmonizer\MasterHarmonizerFactory;

/**
 * Service used to access the module's main functionality.
 *
 * @package Drupal\harmonize\Service
 */
final class Harmonize {

  /**
   * Helpers class.
   *
   * Provides helper functions specific to Harmonizer functionality.
   *
   * @var \Drupal\harmonize\Service\Helpers
   */
  public $helpers;

  /**
   * Drupal's Theme Manager injected through DI.
   *
   * @var \Drupal\Core\Theme\ThemeManager
   */
  public $themeManager;

  /**
   * Drupal's module handler injected through DI.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  public $moduleHandler;

  /**
   * Drupal's event dispatcher injected through DI.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcher
   */
  public $eventDispatcher;

  /**
   * Use DI to inject Drupal's configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  public $configFactory;

  /**
   * Drupal's Admin Context router service injected through DI.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  public $adminContext;

  /**
   * Drupal's Current Route Match service injected through DI.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  public $currentRouteMatch;

  /**
   * Harmonizer cache factory service injected through DI.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  public $cacheBackend;

  /**
   * Harmonizer Language Manager service injected through DI.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  public $languageManager;

  /**
   * Global configuration determined on page load.
   *
   * @var array
   */
  public $config;

  /**
   * Service that contains all functionality for the harmonizer module.
   *
   * This service manages the creation and loading of all harmonizers.
   *
   * @param \Drupal\harmonize\Service\Helpers $harmonizerHelpers
   *   Harmonizer Helpers service injected through DI.
   * @param \Drupal\Core\Theme\ThemeManager $themeManager
   *   Theme Manager service injected through DI.
   * @param \Drupal\Core\Extension\ModuleHandler $moduleHandler
   *   Module Handler service injected through DI.
   * @param \Symfony\Component\EventDispatcher\EventDispatcher|\Drupal\webprofiler\EventDispatcher\TraceableEventDispatcher $eventDispatcher
   *   Event Dispatcher service injected through DI.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Configuration Factory service injected through DI.
   * @param \Drupal\Core\Routing\AdminContext $adminContext
   *   Admin Context service injected through DI.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   Current Route Match service injected through DI.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Harmonizer cache factory service injected through DI.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language Manager service injected through DI.
   */
  public function __construct(
    Helpers $harmonizerHelpers,
    ThemeManager $themeManager,
    ModuleHandler $moduleHandler,
    $eventDispatcher,
    ConfigFactory $configFactory,
    AdminContext $adminContext,
    CurrentRouteMatch $currentRouteMatch,
    CacheBackendInterface $cacheBackend,
    LanguageManagerInterface $languageManager
  ) {
    $this->helpers = $harmonizerHelpers;
    $this->themeManager = $themeManager;
    $this->moduleHandler = $moduleHandler;
    $this->eventDispatcher = $eventDispatcher;
    $this->configFactory = $configFactory;
    $this->adminContext = $adminContext;
    $this->currentRouteMatch = $currentRouteMatch;
    $this->cacheBackend = $cacheBackend;
    $this->languageManager = $languageManager;
    $this->prepare();
  }

  /**
   * Harmonizes an object, providing it's values in a neatly organized array.
   *
   * This function uses the MasterHarmonizerFactory to build a harmonizer for
   * the provided object. This Master Factory then calls sub-factories, to
   * determine the best type of Harmonizer class for the job.
   *
   * @param mixed $object
   *   Object that will be harmonized.
   * @param array $flags
   *   Flags to set on the harmonizers that alter harmonizer behavior.
   *
   * @return mixed
   *   Result of the harmonization process. Returns data if successful.
   */
  public function harmonize($object, array $flags = []) {

    // Only run harmonization if it's activated.
    if (!$this->isPowered()) {
      return [];
    }

    // Fetch/build the harmonizer for this object.
    /* @var \Drupal\harmonize\Harmonizer\HarmonizerInterface $harmonizer */
    $harmonizer = $this->getHarmonizer($object, $flags);

    // Return nothing if the harmonizer could not be loaded.
    if ($harmonizer === NULL) {
      return [];
    }

    // Obtain harmonized data.
    $data = $harmonizer->harmonize();

    // Return the data.
    return $data;
  }

  /**
   * Harmonizes an object, providing it's values in a neatly organized array.
   *
   * This checks if the Auto Pre-Process functionality is activated.
   *
   * @param mixed $object
   *   Object that will be harmonized.
   * @param array $flags
   *   Flags to set on the harmonizers that alter harmonizer behavior.
   *
   * @return mixed
   *   Result of the harmonization process. Returns data if successful.
   */
  public function autoHarmonize($object, array $flags = []) {
    if (isset($this->config[HarmonizeConfig::AUTO_PREPROCESS]) && $this->config[HarmonizeConfig::AUTO_PREPROCESS] === 1) {
      return $this->harmonize($object, $flags);
    }

    return [];
  }

  /**
   * Fetch harmonizer using the Master Factory.
   *
   * @param mixed $object
   *   Object to fetch a harmonizer for.
   * @param array $flags
   *   Flags that affect the actions taken when harmonizing the object.
   *
   * @return mixed|null
   *   The created harmonizer if successful. Returns NULL otherwise.
   */
  public function getHarmonizer($object, array $flags = []) {
    // Build the harmonizer appropriate harmonizer for the given object.
    return MasterHarmonizerFactory::build($object, $flags, $this);
  }

  /**
   * Build the service's properties.
   */
  private function prepare() : void {

    // Only do stuff if the harmonizer is activated.
    if (!$this->isPowered()) {
      return;
    }

    // Set Global Configurations once upon construction.
    $this->config[HarmonizeConfig::HARMONY_FILE_DISCOVERY] = $this->configFactory->get(HarmonizeConfig::MAIN_CONFIG_NAME)->get(HarmonizeConfig::HARMONY_FILE_DISCOVERY) ?? 0;
    $this->config[HarmonizeConfig::AUTO_PREPROCESS] = $this->configFactory->get(HarmonizeConfig::MAIN_CONFIG_NAME)->get(HarmonizeConfig::AUTO_PREPROCESS) ?? 0;
    $this->config[HarmonizeConfig::REMOVE_FIELD_UNDERSCORE_PREFIX] = $this->configFactory->get(HarmonizeConfig::MAIN_CONFIG_NAME)->get(HarmonizeConfig::REMOVE_FIELD_UNDERSCORE_PREFIX) ?? 0;
    $this->config[HarmonizeConfig::MAX_RECURSIVE_DEPTH] = $this->configFactory->get(HarmonizeConfig::MAIN_CONFIG_NAME)->get(HarmonizeConfig::MAX_RECURSIVE_DEPTH) ?? 3;
    $this->config[HarmonizeConfig::CACHE_ENABLED] = $this->configFactory->get(HarmonizeConfig::CACHE_CONFIG_NAME)->get(HarmonizeConfig::CACHE_ENABLED) ?? 1;
    $this->config[HarmonizeConfig::CACHE_EXCLUSIONS] = \array_map('trim', \explode("\n", $this->configFactory->get(HarmonizeConfig::CACHE_CONFIG_NAME)->get(HarmonizeConfig::CACHE_EXCLUSIONS))) ?? [];
  }

  /**
   * Determines whether or not the Harmonizer should run.
   *
   * Certain conditions will disable it completely for certain pages.
   *
   * @return bool
   *   Return boolean stating if the harmonizer is activated.
   */
  private function isPowered() : bool {

    // If it already ran, return the stored value.
    if (isset($this->config[HarmonizeConfig::POWERED])) {
      return $this->config[HarmonizeConfig::POWERED];
    }

    // Initialize the flag variable. By default, the harmonizer is on.
    $powered = TRUE;

    // Disable harmonization in admin context.
    if ($this->adminContext->isAdminRoute($this->currentRouteMatch->getRouteObject())) {
      $powered = FALSE;
    }

    // Set the flag to the Manager's configuration array.
    $this->config[HarmonizeConfig::POWERED] = $powered;

    return $powered;
  }

}
