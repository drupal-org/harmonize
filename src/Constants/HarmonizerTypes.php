<?php

namespace Drupal\harmonize\Constants;

/**
 * Defines types for the harmonization system.
 */
final class HarmonizerTypes {

  /**
   * Global string defining Harmonizers that preprocess Drupal "Entities".
   *
   * @var string
   */
  public const ENTITY = 'entity';

  /**
   * Global string defining Harmonizers that preprocess Drupal "Fields".
   *
   * @var string
   */
  public const ENTITY_FIELD = 'field';

  /**
   * Global string defining Harmonizers that preprocess Drupal "Menus".
   *
   * @var string
   */
  public const MENU = 'menu';

  /**
   * Global string defining Harmonizers that preprocess Drupal "Forms".
   *
   * @var string
   */
  public const FORM = 'form';

  /**
   * Global string defining Harmonizers that preprocess Drupal "Regions".
   *
   * @var string
   */
  public const REGION = 'region';

}
