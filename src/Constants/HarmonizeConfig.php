<?php

namespace Drupal\harmonize\Constants;

/**
 * Defines constants for the harmonization system.
 */
final class HarmonizeConfig {

  /**
   * Global string defining the name for the main configuration of the module.
   *
   * @var string
   */
  public const MAIN_CONFIG_NAME = 'harmonize.settings';

  /**
   * Global string defining the name for the exclusions configurations.
   *
   * @var string
   */
  public const DATA_CONFIG_NAME = 'harmonize.data_configurations';

  /**
   * Global string defining the name for the cache configurations.
   *
   * @var string
   */
  public const CACHE_CONFIG_NAME = 'harmonize.cache_configurations';

  /**
   * Global string defining the name for the exclusions configurations.
   *
   * @var string
   */
  public const STYLES_CONFIG_NAME = 'harmonize.styles_configurations';

  /**
   * String key for module config to determine if it's on.
   *
   * @var string
   */
  public const POWERED = 'powered';

  /**
   * Auto Pre-Processor config string.
   *
   * @var string
   */
  public const AUTO_PREPROCESS = 'auto_preprocess';

  /**
   * Harmony config string.
   *
   * @var string
   */
  public const HARMONY_FILE_DISCOVERY = 'harmony_file_discovery';

  /**
   * Cache enabled config string.
   *
   * @var string
   */
  public const CACHE_ENABLED = 'cache_enabled';

  /**
   * Cache exclusions config string.
   *
   * @var string
   */
  public const CACHE_EXCLUSIONS = 'cache_exclusions';

  /**
   * Remove field underscore prefix config string.
   *
   * @var string
   */
  public const REMOVE_FIELD_UNDERSCORE_PREFIX = 'remove_field_underscore_prefix';

  /**
   * Configure max iteration of recursive preprocessing in entity harmonization.
   *
   * @var string
   */
  public const MAX_RECURSIVE_DEPTH = 'max_recursive_depth';

  /**
   * Entity Settings config string.
   *
   * @var string
   */
  public const ENTITY_TYPE_SETTINGS = 'harmonize_entity_settings';

  /**
   * Entity excluded fields key config string.
   *
   * @var string
   */
  public const ENTITY_TYPE_EXCLUDED_FIELDS = 'harmonize_entity_type_excluded_fields';

  /**
   * Entity bundles excluded fields key config string.
   *
   * @var string
   */
  public const ENTITY_TYPE_BUNDLE_EXCLUDED_FIELDS = 'harmonize_entity_type_bundle_excluded_fields';

  /**
   * Menu Settings config string.
   *
   * @var string
   */
  public const MENU_SETTINGS = 'menu_settings';

  /**
   * Region Settings config string.
   *
   * @var string
   */
  public const REGION_SETTINGS = 'region_settings';

}
