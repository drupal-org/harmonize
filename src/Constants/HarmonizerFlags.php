<?php

namespace Drupal\harmonize\Constants;

/**
 * Defines constants for the harmonizer flags system.
 */
final class HarmonizerFlags {

  /**
   * Global string defining the name of a specific harmonizer flag.
   *
   * SKIP_ENTITY_REFERENCES skips all entity_reference fields in pre-processing.
   */
  public const SKIP_ENTITY_REFERENCES = 'skip_entity_refs';

  /**
   * Global string defining the name of a specific harmonizer flag.
   *
   * LANG can be used to request returning data in a specified language.
   */
  public const LANG = 'lang';

  /**
   * Global string defining the name of a specific harmonizer flag.
   *
   * STYLE can be used to request returning data in a specified style.
   */
  public const STYLE = 'style';

  /**
   * Global string defining the name of a specific harmonizer flag.
   *
   * PARENT can be used to set a parent to a harmonizer.
   */
  public const PARENT = 'parent';

  /**
   * Global string defining the name of a specific harmonizer flag.
   *
   * NO_RECURSIVE can be used to prohibit recursive harmonization.
   * If this flag is used, instead of harmonizing any nested entities, the data
   * will instead include the harmonizer itself.
   */
  public const NO_RECURSIVE = 'no_recursive';

}
