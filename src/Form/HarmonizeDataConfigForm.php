<?php

namespace Drupal\harmonize\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\harmonize\Constants\HarmonizeConfig;
use Drupal\harmonize\Service\Helpers;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a configuration form for the Harmonizer module.
 *
 * @TODO - This whole form needs a revamp! My goodness.
 *
 * @package Drupal\harmonize\Form
 */
class HarmonizeDataConfigForm extends ConfigFormBase {

  /**
   * Helpers service injected through DI.
   *
   * @var \Drupal\harmonize\Service\Helpers
   */
  private $helpers;

  /**
   * HarmonizeConfigForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal's Configuration Factory injected through DI.
   * @param \Drupal\harmonize\Service\Helpers $helpers
   *   Harmonizer Helpers service injected through DI.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Helpers $helpers) {
    parent::__construct($config_factory);
    $this->helpers = $helpers;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('config.factory'),
      $container->get('harmonize.helpers')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'harmonizer_admin_data_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() : array {
    return [
      HarmonizeConfig::DATA_CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) : array {

    // Set this for better organization of form values later.
    $form['#tree'] = TRUE;

    // Attach menu configuration fields.
    $this->attachMenusForm($form);

    // Attach region configuration fields.
    $this->attachRegionsForm($form);

    // Attach entity configuration fields.
    // @todo - This will be changed to exclude certain content types altogether.
    $this->attachEntitiesForm($form);

    // Return the form with all necessary fields.
    return parent::buildForm($form, $form_state);
  }

  /**
   * Add form fields allowing exclusion of certain fields from harmonization.
   *
   * @param mixed $form
   *   Form to attach new elements to.
   */
  public function attachEntitiesForm(&$form): void {

    // Retrieve current values from site configuration.
    $entity_settings = $this->config(HarmonizeConfig::DATA_CONFIG_NAME)->get(HarmonizeConfig::ENTITY_TYPE_SETTINGS);

    // Get the site's entity bundles.
    $entity_bundles = $this->helpers->getSiteEntityBundles();

    // This part of the form handles the concept of filtering in harmonizers.
    // Instantiate a Vertical Tabs collection to organize the form.
    $form[HarmonizeConfig::ENTITY_TYPE_SETTINGS] = [
      '#type'        => 'details',
      '#title'       => t('Exclusion Settings for Entities'),
      '#description' => t(
        'By default, when using the harmonizer on an Entity, it pre-processes all fields in the entity. You can filter which fields are pre-processed and omit certain fields.<br>It is recommended to exclude fields that may produce a lot of overhead or are prone to inducing infinite loops.'
      ),
      '#weight'      => 1,
      '#open'        => TRUE,
    ];

    // Loop in all entity bundles.
    // Create a form for each entity type and bundle.
    foreach ($entity_bundles as $entity_type_id => $bundles) {

      // Add a details section for the Entity Type.
      $form[HarmonizeConfig::ENTITY_TYPE_SETTINGS][$entity_type_id] = [
        '#type'  => 'details',
        '#title' => t(
          '<strong>@entity_type_id</strong>',
          ['@entity_type_id' => ucwords($entity_type_id) . 's']
        ),
        '#description' => t(
          '<br>Enable exclusions on fields for all entities of this type here, or exclude fields for certain bundles further below.<br>'
        ),
      ];

      // Instantiate a fieldset for the fields of the entity type.
      $form[HarmonizeConfig::ENTITY_TYPE_SETTINGS][$entity_type_id][HarmonizeConfig::ENTITY_TYPE_EXCLUDED_FIELDS] = [
        '#type'        => 'fieldset',
        '#title'       => t(
          'Fields to exclude for the <strong>@entity_type_id</strong> entity type',
          ['@entity_type_id' => ucwords($entity_type_id)]
        ),
        '#description' => t(
          '<br><strong>Fields excluded at this level will be excluded for harmonization in all entities of this type.</strong>'
        ),
        '#collapsible' => TRUE,
        '#collapsed'   => TRUE,
        '#tree'        => TRUE,
      ];

      // Get field definitions for the given bundle.
      $entity_type_field_configs = $this->helpers->getEntityTypeFieldDefinitions($entity_type_id);

      // Checkboxes to exclude harmonization of fields for an entity type.
      /* @var integer $id */
      /* @var \Drupal\field\FieldStorageConfigInterface $entity_type_field_config */
      foreach ($entity_type_field_configs as $id => $entity_type_field_config) {
        $entity_type_field_configs[$id]
          = "{$entity_type_field_config->getLabel()} <strong>[{$entity_type_field_config->getName()}]</strong> ({$entity_type_field_config->getType()})";
      }
      // Configure which fields should be pre-processed.
      $form[HarmonizeConfig::ENTITY_TYPE_SETTINGS][$entity_type_id][HarmonizeConfig::ENTITY_TYPE_EXCLUDED_FIELDS] = [
        '#type'          => 'checkboxes',
        '#options'       => $entity_type_field_configs,
        '#default_value' => !empty($entity_settings[$entity_type_id][HarmonizeConfig::ENTITY_TYPE_EXCLUDED_FIELDS]) ? $entity_settings[$entity_type_id][HarmonizeConfig::ENTITY_TYPE_EXCLUDED_FIELDS] : [],
      ];

      // Loop in all bundles for the given entity type.
      foreach ($bundles as $bundle => $label) {

        // Add a details section for the Entity Bundle.
        $form[HarmonizeConfig::ENTITY_TYPE_SETTINGS][$entity_type_id][$bundle] = [
          '#type'  => 'details',
          '#title' => t(
            '<strong>@bundle</strong>', [
              '@bundle' => ucwords(
                str_replace('_', ' ', $bundle)
              ),
            ]
          ),
        ];

        // Instantiate a fieldset for the fields of the bundle.
        // This fieldset is only visible when the above checkbox is checked.
        $form[HarmonizeConfig::ENTITY_TYPE_SETTINGS][$entity_type_id][$bundle][HarmonizeConfig::ENTITY_TYPE_BUNDLE_EXCLUDED_FIELDS] = [
          '#type'        => 'fieldset',
          '#title'       => t(
            'Fields to exclude for <strong>@bundle</strong>',
            ['@bundle' => ucwords($label)]
          ),
          '#collapsible' => TRUE,
          '#collapsed'   => TRUE,
          '#tree'        => TRUE,
          '#states'      => [
            'invisible' => [
              ":input[name=\"entity_settings[{$entity_type_id}][{$bundle}][enabled]\"]" => ['checked' => FALSE],
            ],
          ],
        ];

        // Get field definitions for the given bundle.
        $entity_type_bundle_field_configs = $this->helpers->getEntityTypeBundleFieldDefinitions($entity_type_id, $bundle);

        /* @var integer $id */
        /* @var \Drupal\field\FieldConfigInterface $entity_type_bundle_field_config */
        foreach ($entity_type_bundle_field_configs as $id => $entity_type_bundle_field_config) {
          $entity_type_bundle_field_configs[$id]
            = "Exclude {$entity_type_bundle_field_config->getLabel()} <strong>[{$entity_type_bundle_field_config->getName()}]</strong> ({$entity_type_bundle_field_config->getType()})";
        }

        // Configure which fields should be pre-processed.
        $form[HarmonizeConfig::ENTITY_TYPE_SETTINGS][$entity_type_id][$bundle][HarmonizeConfig::ENTITY_TYPE_BUNDLE_EXCLUDED_FIELDS] = [
          '#type'          => 'checkboxes',
          '#options'       => $entity_type_bundle_field_configs,
          '#default_value' => !empty($entity_settings[$entity_type_id][$bundle][HarmonizeConfig::ENTITY_TYPE_BUNDLE_EXCLUDED_FIELDS]) ? $entity_settings[$entity_type_id][$bundle][HarmonizeConfig::ENTITY_TYPE_BUNDLE_EXCLUDED_FIELDS] : [],
        ];
      }
    }
  }

  /**
   * Add form fields allowing exclusion of certain menus from harmonization.
   *
   * @param mixed $form
   *   Form to attach new elements to.
   */
  public function attachMenusForm(&$form): void {

    // Retrieve current values from site configuration.
    $menu_settings = $this->config(HarmonizeConfig::DATA_CONFIG_NAME)->get(HarmonizeConfig::MENU_SETTINGS);

    // Get the site's custom menus.
    $custom_menus = menu_ui_get_menus('true');

    // This part of the form handles the concept of harmonization Menus.
    $form[HarmonizeConfig::MENU_SETTINGS] = [
      '#type'  => 'details',
      '#title' => t('Enable harmonization for select Menus'),
      '#description' => t('Menu harmonization can be very heavy. As a result, harmonization for all menus is disabled by default. You can enable harmonization for select menus here.'),
      '#weight'      => -1,
      '#open'        => TRUE,
    ];

    // Loop in the list of menu names to set parts of the form.
    foreach ($custom_menus as $id => $name) {
      // Add a checkbox to enable harmonization for the given menu.
      $form[HarmonizeConfig::MENU_SETTINGS][$id] = [
        '#type'          => 'checkbox',
        '#title'         => t(
          'Enable harmonization for the <strong>@id</strong> menu', ['@id' => $id]
        ),
        '#default_value' => !empty($menu_settings[$id]) ? $menu_settings[$id] : 0,
      ];
    }
  }

  /**
   * Add form fields allowing exclusion of certain regions from harmonization.
   *
   * @param mixed $form
   *   Form to attach new elements to.
   */
  public function attachRegionsForm(&$form): void {

    // Retrieve current values from site configuration.
    $region_settings = $this->config(HarmonizeConfig::DATA_CONFIG_NAME)->get(HarmonizeConfig::REGION_SETTINGS);

    // Get the site's main regions.
    $defaultTheme = $this->config('system.theme')->get('default');
    $regions = system_region_list($defaultTheme, TRUE);

    // This part of the form handles the concept of harmonization Menus.
    $form[HarmonizeConfig::REGION_SETTINGS] = [
      '#type'  => 'details',
      '#title' => t('Enable harmonization for select Regions'),
      '#description' => t('Region harmonization can be very heavy. As a result, harmonization for all regions is disabled by default. You can enable harmonization for select regions here.'),
      '#weight'      => 0,
      '#open'        => TRUE,
    ];

    // Loop in the list of menu names to set parts of the form.
    foreach ($regions as $id => $name) {
      // Add a checkbox to enable harmonization for the given menu.
      $form[HarmonizeConfig::REGION_SETTINGS][$id] = [
        '#type'          => 'checkbox',
        '#title'         => t(
          'Enable harmonization for the <strong>@id</strong> region', ['@id' => $id]
        ),
        '#default_value' => !empty($region_settings[$id]) ? $region_settings[$id] : 0,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) : void {
    // Retrieve the configuration.
    $values = $form_state->getValues();

    unset($values['actions']);

    $this->configFactory->getEditable(HarmonizeConfig::DATA_CONFIG_NAME)
      ->set(HarmonizeConfig::ENTITY_TYPE_SETTINGS, $values[HarmonizeConfig::ENTITY_TYPE_SETTINGS])
      ->set(HarmonizeConfig::MENU_SETTINGS, $values[HarmonizeConfig::MENU_SETTINGS])
      ->set(HarmonizeConfig::REGION_SETTINGS, $values[HarmonizeConfig::REGION_SETTINGS])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
