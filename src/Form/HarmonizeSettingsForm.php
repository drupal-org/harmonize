<?php

namespace Drupal\harmonize\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\harmonize\Constants\HarmonizeConfig;

/**
 * Provides a configuration form for the Harmonizer module.
 *
 * @package Drupal\harmonize\Form
 */
class HarmonizeSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'harmonizer_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() : array {
    return [
      HarmonizeConfig::MAIN_CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) : array {

    // Set this for better organization of form values later.
    $form['#tree'] = TRUE;

    // Get the current configurations.
    $auto_preprocess = $this->config(HarmonizeConfig::MAIN_CONFIG_NAME)->get(HarmonizeConfig::AUTO_PREPROCESS);
    $harmony_file_discovery = $this->config(HarmonizeConfig::MAIN_CONFIG_NAME)->get(HarmonizeConfig::HARMONY_FILE_DISCOVERY);
    $remove_field_underscore_prefix = $this->config(HarmonizeConfig::MAIN_CONFIG_NAME)->get(HarmonizeConfig::REMOVE_FIELD_UNDERSCORE_PREFIX);
    $max_recursive_depth = $this->config(HarmonizeConfig::MAIN_CONFIG_NAME)->get(HarmonizeConfig::MAX_RECURSIVE_DEPTH);

    // Enable the Auto-Preprocessor.
    $form[HarmonizeConfig::AUTO_PREPROCESS] = [
      '#type'          => 'checkbox',
      '#title'         => t('Enable Automatic Preprocessing'),
      '#description'   => t(
        'Check this box to automatically handle preprocessing of <strong>Nodes, Paragraphs, Media, Regions & Menus</strong>.<br>
This will add a <strong>data</strong> variable to their templates that contains harmonized data. Checking this box prevents you from having to manually create preprocess hooks to add the data to templates.'
      ),
      '#default_value' => !empty($auto_preprocess) ? $auto_preprocess : 0,
      '#weight'        => -15,
    ];

    // Enable the use of Harmony.
    $form[HarmonizeConfig::HARMONY_FILE_DISCOVERY] = [
      '#type'          => 'checkbox',
      '#title'         => t('Enable Harmony File Discovery'),
      '#description'   => t(
        'Check this box to enable Harmony File Discovery. Refer to the help page or the README.md in the module for more details!'
      ),
      '#default_value' => !empty($harmony_file_discovery) ? $harmony_file_discovery : 0,
      '#weight'        => -15,
    ];

    // Enable the automatic removal "field_" prefix when harmonization fields.
    $form[HarmonizeConfig::REMOVE_FIELD_UNDERSCORE_PREFIX] = [
      '#type'          => 'checkbox',
      '#title'         => t('Remove <strong>"field_"</strong> prefix when harmonizing Entity Fields.'),
      '#description'   => t(
        'Check this box to remove the prefix from field names when harmonizing entity fields. Array keys for specific fields will have the full field name otherwise.'
      ),
      '#default_value' => !empty($remove_field_underscore_prefix) ? $remove_field_underscore_prefix : 0,
      '#weight'        => -15,
    ];

    // Enable the automatic removal "field_" prefix when harmonization fields.
    $form[HarmonizeConfig::MAX_RECURSIVE_DEPTH] = [
      '#type'          => 'select',
      '#title'         => t('Select the max depth for recursive harmonization in Entities.'),
      '#description'   => t(
        'A failsafe is developed to prevent infinite loops when harmonizing entity fields, since entity fields can reference other entities with <code>entity_reference</code> type fields.<br>
After reaching the max depth, the harmonized data will be replaced harmonizer classes. You can then call the harmonize() function manually on that class to obtain the data desired.'
      ),
      '#options'       => [
        0 => 0,
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
      ],
      '#default_value' => !empty($max_recursive_depth) ? $max_recursive_depth : 3,
      '#weight'        => -15,
    ];

    // Return the form with all necessary fields.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) : void {
    // Retrieve the configuration.
    $values = $form_state->getValues();

    unset($values['actions']);

    $this->configFactory->getEditable(HarmonizeConfig::MAIN_CONFIG_NAME)
      ->set(HarmonizeConfig::AUTO_PREPROCESS, $values[HarmonizeConfig::AUTO_PREPROCESS])
      ->set(HarmonizeConfig::HARMONY_FILE_DISCOVERY, $values[HarmonizeConfig::HARMONY_FILE_DISCOVERY])
      ->set(HarmonizeConfig::REMOVE_FIELD_UNDERSCORE_PREFIX, $values[HarmonizeConfig::REMOVE_FIELD_UNDERSCORE_PREFIX])
      ->set(HarmonizeConfig::MAX_RECURSIVE_DEPTH, $values[HarmonizeConfig::MAX_RECURSIVE_DEPTH])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
