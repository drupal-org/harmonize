<?php

namespace Drupal\harmonize\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\harmonize\Constants\HarmonizeConfig;

/**
 * Provides a configuration form for the Harmonizer module.
 *
 * @package Drupal\harmonize\Form
 */
class HarmonizeCacheConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'harmonizer_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() : array {
    return [
      HarmonizeConfig::MAIN_CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) : array {

    // Set this for better organization of form values later.
    $form['#tree'] = TRUE;

    // Get the current configurations.
    $cache_enabled = $this->config(HarmonizeConfig::CACHE_CONFIG_NAME)->get(HarmonizeConfig::CACHE_ENABLED);
    $cache_exclusions = $this->config(HarmonizeConfig::CACHE_CONFIG_NAME)->get(HarmonizeConfig::CACHE_EXCLUSIONS);

    // Enable Caching.
    $form[HarmonizeConfig::CACHE_ENABLED] = [
      '#type'          => 'checkbox',
      '#title'         => t('Enable Harmonize Cache <strong>(Experimental)</strong>'),
      '#description'   => t(
        'Check this box to cache harmonizer data results. This will reduce page processing time.<br>
<strong>This is still an experimental feature and may not provide desired results. Use at your own risk!</strong>'
      ),
      '#default_value' => $cache_enabled,
      '#weight'        => -15,
    ];

    // @TODO - Only show this if the above is checked!
    $form[HarmonizeConfig::CACHE_EXCLUSIONS] = [
      '#type'          => 'textarea',
      '#title'         => t('Disable caching for specific harmonizers'),
      '#description'   => t(
        'On each line, enter the signature of a harmonizer you would like to disable caching for. <em>i.e. <strong>entity.node.page</strong></em>'
      ),
      '#default_value' => !empty($cache_exclusions) ? $cache_exclusions : '',
      '#weight'        => -15,
    ];

    // Return the form with all necessary fields.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) : void {
    // Retrieve the configuration.
    $values = $form_state->getValues();

    unset($values['actions']);

    $this->configFactory->getEditable(HarmonizeConfig::CACHE_CONFIG_NAME)
      ->set(HarmonizeConfig::CACHE_ENABLED, $values[HarmonizeConfig::CACHE_ENABLED])
      ->set(HarmonizeConfig::CACHE_EXCLUSIONS, $values[HarmonizeConfig::CACHE_EXCLUSIONS])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
