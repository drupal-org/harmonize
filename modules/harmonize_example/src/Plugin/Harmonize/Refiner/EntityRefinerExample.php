<?php

namespace Drupal\harmonize_examples\Plugin\Harmonizer\Refiner;

use Drupal\harmonize\PluginManager\Refinery\RefinerBase;

/**
 * Provide plugin to alter harmonizer data for all Entities.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Refiner(
 *   id = "harmonize_example.entity_refiner",
 *   target = "entity"
 * )
 *
 * @package Drupal\harmonize\Plugin\Harmonizer\Refiner
 */
final class EntityRefinerExample extends RefinerBase {

  /**
   * Add personal tweaks to data in this function.
   *
   * {@inheritdoc}
   */
  public function refine(&$consignment, &$harmony, $entity): void {
    // Alter Harmonization Data here!
  }

}
