<?php

namespace Drupal\harmonize_examples\Plugin\Harmonizer\Refiner;

use Drupal\harmonize\PluginManager\Refinery\RefinerBase;

/**
 * Provide plugin to alter harmonizer data for String type (field_plain_text).
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Refiner(
 *   id = "harmonize_example.field_plain_text_refiner",
 *   target = "field.string.field_plain_text"
 * )
 *
 * @package Drupal\harmonize\Plugin\Harmonizer\Refiner
 */
final class FieldPlainTextRefinerExample extends RefinerBase {

  /**
   * Add personal tweaks to data in this function.
   *
   * {@inheritdoc}
   */
  public function refine(&$consignment, &$harmony, $node): void {
    // Alter Harmonization Data here!
  }

}
