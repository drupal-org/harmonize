<?php

namespace Drupal\harmonize_examples\Plugin\Harmonizer\Refiner;

use Drupal\harmonize\PluginManager\Refinery\RefinerBase;

/**
 * Provide plugin to alter harmonizer data for all Email Type fields.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Refiner(
 *   id = "harmonize_example.field_email_refiner",
 *   target = "field.email"
 * )
 *
 * @package Drupal\harmonize\Plugin\Harmonizer\Refiner
 */
final class EmailFieldRefinerExample extends RefinerBase {

  /**
   * Add personal tweaks to data in this function.
   *
   * {@inheritdoc}
   */
  public function refine(&$consignment, &$harmony, $field): void {
    // Alter Harmonization Data here!
  }

}
