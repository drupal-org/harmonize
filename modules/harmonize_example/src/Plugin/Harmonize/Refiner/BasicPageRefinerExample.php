<?php

namespace Drupal\harmonize_example\Plugin\Harmonizer\Refiner;

use Drupal\harmonize\PluginManager\Refinery\RefinerBase;

/**
 * Provide plugin to alter harmonizer data for all Basic Pages.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Refiner(
 *   id = "harmonize_example.basic_page_refiner",
 *   target = "entity.node.basic_page"
 * )
 *
 * @package Drupal\harmonize\Plugin\Harmonizer\Refiner
 */
final class BasicPageRefinerExample extends RefinerBase {

  /**
   * Add personal tweaks to data in this function.
   *
   * {@inheritdoc}
   */
  public function refine(&$consignment, &$harmony, $node): void {
    // Alter Harmonization Data here!
  }

}
