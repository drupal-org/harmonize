Use this directory if you are making use of the Harmonize Module's methodology.

If the Harmonizer Module is not being used, this directory can safely be removed.
