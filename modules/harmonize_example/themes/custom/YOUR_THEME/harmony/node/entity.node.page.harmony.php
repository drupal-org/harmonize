<?php

/**
 * @file
 * Override data for a given groomer.
 *
 * @groomer_type entity
 * @entity_type node
 * @bundle page
 *
 * This is a custom file to handle the reformatting of data before it reaches
 * Drupal templates.
 *
 * Available variables:
 * @var array $groomerData
 *    The data coming from the groomer.
 * @var array $data
 *    The data that will be returned to the Twig template.
 *
 * Re-arrange and assign data to the $data variable here, and that's all you
 * need. The rest will be handled in the Twig Templates.
 */
